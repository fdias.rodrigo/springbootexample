package br.com.example.SpringBootExample.service.converter;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import br.com.example.SpringBootExample.controller.api.PersonDTO;
import br.com.example.SpringBootExample.entity.Person;

@Component
public class PersonEntityConverter {

	public PersonDTO toDTO(Person person) {
		
		Assert.notNull(person);
		
		PersonDTO personDTO = new PersonDTO();
		personDTO.setName(person.getFullName());
		personDTO.setBirthDay(person.getDateOfBirth());
		return personDTO;
	}
	
}
