package br.com.example.SpringBootExample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.example.SpringBootExample.controller.api.PersonDTO;
import br.com.example.SpringBootExample.service.PersonService;

@Controller
public class CadastroController {

	private static final String BASEPATH = "/persons";
	
    @Autowired
    private PersonService personService;
 
    @ResponseBody
    @RequestMapping(BASEPATH)
    public String index() {
        Iterable<PersonDTO> all = personService.findAll();
        StringBuilder sb = new StringBuilder();
        all.forEach(p -> sb.append(p.getName() + "<br>"));
        return sb.toString();
    }
    
    @PostMapping(BASEPATH)
    public void savePerson(PersonDTO personDTO) {
    	
    }
    
}
