package br.com.example.SpringBootExample.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.example.SpringBootExample.controller.api.PersonDTO;
import br.com.example.SpringBootExample.dao.PersonDAO;
import br.com.example.SpringBootExample.service.converter.PersonEntityConverter;

@Service
public class PersonService {
	
	private PersonDAO personDAO;
	private PersonEntityConverter personEntityConverter;
	
	public PersonService(PersonDAO personDAO, PersonEntityConverter personEntityConverter) {
		this.personDAO = personDAO;
		this.personEntityConverter = personEntityConverter;
	}
	
	public List<PersonDTO> findAll(){
		
		return personDAO.findAll()
				.stream().map(p->personEntityConverter.toDTO(p)).collect(Collectors.toList());
	}

}
